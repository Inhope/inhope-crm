$(function () {
    function showChat() {
        $('.chat-ctn').show();
        setTimeout(function () {
            $('body').addClass('chatting');
        }, 100);
    }
    function hideChat() {
        $('body').removeClass('chatting');
        setTimeout(function () {
            $('.chat-ctn').hide();
        }, 500);
    }

    $chatInput = $('#chat-input');

    var selcetedCateId = '';
    var $cateSelected = $('.cate-selected');
    $('.cate-select .check-ctn li').click(function () {
        selcetedCateId = $(this).data('id');
        $cateSelected.text($.trim($(this).text()));
    });

    $('#xiaoi-btn').click(function () {
        showChat();
        $chatInput.focus();
    });

    $('.chat-out').click(function () {
        hideChat();
    });





    $("#support-btn").click(supportShow);//客户支持对话框出现
    $(".close-support").click(supportHide);//客户支持对话框消失
    $("#fileInput").change(fileNameShow);//选中文件后显示
    $(".submitI").click(upload);//提交事件
    $("#emailI").blur(function() {
        blurNothing(1);
    })
    $("#mobileI").blur(function() {
        blurNothing(2);
    })
    $("#contentI").blur(function() {
        blurNothing(3);
    })


    //客户支持对话框出现
    function supportShow() {
        $(".chat-right").animate({
            "right":305
        },500).animate({
            "right":297
        },100).animate({
            "right":300
        },100)
        $(this).addClass("open");
        $(".close-support").removeClass('hidden');
        $(".support-ctn").animate({
            "right":5
        },500).animate({
            "right":-3
        },100).animate({
            "right":0
        },100)
    }
    //客户支持对话框消失
    function supportHide() {
        $(".chat-right").animate({
            "right":-5
        },500).animate({
            "right":3
        },100).animate({
            "right":0
        },100)
        $(this).addClass("hidden");
        $("#support-btn").removeClass("open");
        $(".support-ctn").animate({
            "right":-300
        },500)
    }
    //选中文件，显示名字
    function fileNameShow() {
        // alert($(this).val())
        $(".fileName").html("");
        var str = $(this).val();
        var spa = $("<span></span>");
        console.log(str+"|"+str.lastIndexOf("\\"));
        var a = str.lastIndexOf("\\")+1;
        spa.text( str.slice(a,str.length));
        spa.prependTo($(".fileName"));
    }
    //提交事件
    function upload() {

        if($("#emailI").val() == ""
            ||  !/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($("#emailI").val())
        ) {
            $(".ti1").show();
            $("#emailI").focus();
        }
        else if($("#mobileI").val() == ""
            || !/^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/.test($("#mobileI").val())
        ) {
            $(".ti1").hide();
            $(".ti2").show();
            $("#mobileI").focus();
        }
        else if($("#contentI").val() == "") {
            $(".ti2").hide();
            $(".ti3").show();
            $("#contentI").focus();
        }
        else {
            $(".ti3").hide();
            var params = {
                realName: $("#userNameI").val(),
                email: $("#emailI").val(),
                mobile: $("#mobileI").val(),
                content: $("#contentI").val(),
                file: $('#fileInput').get(0).files[0]
            }

            var form = new FormData();
            if (params) {
                for (var k in params) {
                    form.append(k, params[k]);
                }
            }
            var xhr = new XMLHttpRequest();
            xhr.open("post", '/support', true);
            xhr.onreadystatechange = function () {
                if(4 == xhr.readyState && 200 == xhr.status) {
                    try {
                        var ret = JSON.parse(xhr.responseText);
                        if (ret.ok) {
                            alert('感谢您的提问, 我们会及时回复');
                        }
                        else {
                            alert(ret.msg || '出错');
                        }
                    }catch(e){
                        alert('服务出错');
                    }
                }
            };
            xhr.onload = function (ret) {};
            xhr.send(form);
        }

    }
    //失去焦点判断
    function blurNothing(a) {




        // if(a == 1) {
        //     var pattern =  /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        //     if($("#emailI").val() == ""
        //         || !pattern.test($("#emailI").val())
        //     ) {
        //         $(".ti1").show();
        //     }
        //     else {
        //         $(".ti1").hide();
        //     }
        // }
        // else if(a == 2) {
        //     var pattern =  /^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/;
        //     if($("#mobileI").val() == ""
        //         || !pattern.test($("#mobileI").val())
        //     ) {
        //         $(".ti2").show();
        //     }
        //     else {
        //         $(".ti2").hide();
        //     }
        // }
        // else if(a == 3) {
        //     if($("#contentI").val() == "") {
        //         $(".ti3").show();
        //     }
        //     else {
        //         $(".ti3").hide();
        //     }
        // }
    }


























});