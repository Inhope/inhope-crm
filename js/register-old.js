$(function(){
	var sendOk = false;
	var firstSend = true;
	var hasTimer = false;
	var agreeSec_first = 0;
	var agreeSec_second = 0;
	var agreeSec_third = 0;
	var agreeSec_fourth = 0;
	var agreeSec_fifth = 0;
	var parentId ;
	var childId;

	$(".first input").blur(tipMessage);//提示信息
	$('.sendCode').click(sendCode);//发送验证码
	$("#phoneSub").click(nextTest);//第一页的下一步
	$("#attribute").click(listShow);//下拉列表出现
	$(".compress").click(listShow);//下拉列表出现
	$("#checkBox").click(agree);//去除协议下的提示
	$('body').click(listHidden);//点击别处，下拉列表消失
	$("#sub").click(secondNext);//第二页的下一步
	$('.second_grade li').click(listChecked);//选中二级菜单公司属性
	$(".second input").blur(isNull);//第二页失去焦点判断是否为空
	$("#turnToIndex").click(turnToIndex);//点击回到登录页

	//失去焦点验证
	function tipMessage() {
		var inputIndex = $(this).index("input");//从1-11
		var inputText = $(this).val();
		var paTop = $(this).position().top+$(this).outerHeight()+12;
		switch (inputIndex) {
			case 1://手机验证
				phoneJudge();
				break;
			case 2://验证码
				if(inputText == "") {
					$(this).next().next().html('<span>!</span>请输入验证码');
					$(this).next().next().removeClass('hidden');
				}
				else {
					var tip = $(this).next().next();
					$.post("validToken",{mobile:$(".wrap input").eq(0).val(),token:inputText},function(obj){
						console.log(obj);
						if(!obj.ok){
							tip.html('<span>!</span>验证码错误');
							tip.removeClass("hidden");
							agreeSec_second = 0;
						}else{
							tip.addClass("hidden");
							agreeSec_second = 1;
						}
					})
				}
				break;
			case 3://密码
				var pattern = /^\w{6,}$/;
				$(this).next().html('<span>!</span>请输入至少6位字符的密码');
				if(!pattern.test(inputText)) {
					$(this).next().removeClass('hidden');
				}
				else {
					$(this).next().addClass('hidden');
					agreeSec_third = 1;
				}
				break;
			case 4://确认密码
				$(this).next().html('<span>!</span>两次密码不一致，请您重新输入');
				if(inputText != $('.first input').eq(2).val()) {
					$(this).next().removeClass('hidden');
				}
				else {
					$(this).next().addClass('hidden');
					agreeSec_fourth = 1;
				}
				break;
			case 5://邮箱
				var pattern =  /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				$(this).next().html('<span>!</span>请输入正确的邮箱');
				if(!pattern.test(inputText)) {
					$(this).next().removeClass('hidden');
				}
				else {
					$(this).next().addClass('hidden');
					agreeSec_fifth = 1;
				}
				break;

			default:
				break;
		}
	}

	//发送验证码
	function sendCode() {
		phoneJudge();
		if(hasTimer) {
			$(".tip").eq(1).html('<span>!</span>短信已经发送到您的手机，请查看');
			$(".tip").eq(1).removeClass('hidden');
			return;
		}
		if(sendOk) {
			$(".sendCode").button("loading");
			$.post("/sendTextMessageForRegister"
				,{mobile:$('#mobile').val()}
				,function(obj) {
					console.log(obj);
					$(".sendCode").button("reset");
					if(obj.ok) {
						firstSend = false;
						var count = 61;
						// alert($(this).text());
						$(".sendCode").text("发送成功");
						$(".tip").eq(1).addClass('hidden');
						$(".wrap input").eq(1).val("");
						var timer = setInterval(function(){
							hasTimer = true;
							if(count == 1){
								clearInterval(timer);
								$(".sendCode").text("重新发送");
								sendOk = true;
								hasTimer = false;
								firstSend = true;
								return;
							}
							count--;
							$(".sendCode").text(count+"s  后重新发送");
						},1000)
					}
					else {
						firstSend = false;
						$(".tip").eq(1).html('<span>!</span>'+obj.msg);
						$(".tip").eq(1).removeClass('hidden');
						$(".sendCode").text("获取验证码");
				}
			})
		}
	}

	//第一页下一步验证
	function nextTest(){
		if(!agreeSec_first){
			$(".tip").eq(0).html('<span>!</span>请输入正确的手机号码');
			$(".tip").eq(0).removeClass('hidden');
		}
		if(!agreeSec_second) {
			$(".tip").eq(1).html('<span>!</span>请输入正确的验证码');
			$(".tip").eq(1).removeClass('hidden');
		}
		if(!agreeSec_third) {
			$(".tip").eq(2).html('<span>!</span>请输入至少6位字符的密码');
			$(".tip").eq(2).removeClass('hidden');
		}
		if(!agreeSec_fourth) {
			$(".tip").eq(3).html('<span>!</span>请确认您的密码');
			$(".tip").eq(3).removeClass('hidden');
		}
		if(!agreeSec_fifth) {
			$(".tip").eq(4).html('<span>!</span>请输入正确的邮箱');
			$(".tip").eq(4).removeClass('hidden');
		}
		if(agreeSec_first && agreeSec_second && agreeSec_third && agreeSec_fourth && agreeSec_fifth) {
			if($("#checkBox").is(":checked")) {
				toBeSecndPage();
			}else{
				$(".tip").eq(5).html('<span>!</span>请您仔细阅读信安协议，如无异议，请选中按钮');
				$(".tip").eq(5).removeClass('hidden');
			}
		}
	}

	//下拉列表出现
	function listShow(e){
		e.stopPropagation();
		$(".first_grade").removeClass("hidden");
	}

	//是否同意协议按钮事件
	function agree(){
		if($("#checkBox").is(":checked")){
			$(this).parent().next().addClass('hidden');
		}else{
			$(this).parent().next().html('<span>!</span>请您仔细阅读信安协议，如无异议，请选中按钮');
			$(this).parent().next().removeClass('hidden');
		}
	}

	//下拉列表隐藏
	function listHidden(){
		$(".first_grade").addClass("hidden");
	}

	//第二页下一步验证
	function secondNext(){
		// alert(0)
		for(var i = 0;i<$(".second input").length;i++) {
			if($(".second input").eq(i).val() == "") {
				$(".second input").eq(i).parent().find(".tip").removeClass("hidden");
			}
			else {
				$(".second input").eq(i).parent().find(".tip").addClass("hidden");
			}
		}
		if(
			$('.second input').eq(0).val() != ""
			&& $('.second input').eq(1).val() != ""
			&& $('.second input').eq(2).val() != ""
			&& $('.second input').eq(3).val() != ""
		) {
			$("#sub").button('loading');
			$.post("/doRegister",{
				mobile:$('.first input').eq(0).val(),
				email:$('.first input').eq(4).val(),
				token:$('.first input').eq(1).val(),
				password:$('.first input').eq(2).val(),
				realName:$('.second input').eq(0).val(),
				position:$('.second input').eq(3).val(),
				companyName:$('.second input').eq(1).val(),
				parentAttrId:parentId,
				childAttrId:childId
			},function(obj){
				console.log(obj);
				$("#sub").removeClass('reset');
				if(obj.ok){
					toBeThirdPage()
					countDown();
				}else {
					alert(obj.msg);
					toBeFirstPage();
				}

			})
		}
	}

	//点击二级菜单，选取公司属性
	function listChecked(){
		$("#attribute").val($(this).text());
		childId = $(this).attr("data-id");
		parentId = $(this).parent().parent().attr("data-id");
		$(".ti3").addClass('hidden');
		$('.first_grade').addClass('hidden');
	}

	//第二页失去焦点判断是否为空
	function isNull() {
		var inputText = $(this).val();
		if(inputText == ""){
			$(this).parent().find(".tip").removeClass('hidden');
		}else{
			$(this).parent().find(".tip").addClass("hidden");
		}
	}

	//点击跳转到登录页
	function turnToIndex() {
		window.location.href="/logout";
	}

	//阻止body事件的冒泡
	$(".first-li").click(function (e) {
		e.stopPropagation();
	})

	//5秒后跳转
	function countDown() {
		var count = 5;
		// alert($(this).text());
		$("#turnToIndex").text("正在跳转至首页 "+count+"s");
		var timer = setInterval(function(){
			if(count == 1) {
				clearInterval(timer);
				turnToIndex();
				return;
			}
			count--;
			$("#turnToIndex").text("正在跳转至首页 "+count+"s");
		},1000)
	}

	//手机验证
	function phoneJudge() {
		var pattern = /^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/;
		if(!pattern.test($("#mobile").val())) {
			$(".tiPhone").html('<span>!</span>请输入11位手机号码，用于登录及找回密码');
			$(".tiPhone").removeClass("hidden");
		}else{
			$(".tiPhone").addClass("hidden");
			agreeSec_first = 1;
			sendOk = true;
		}
	}

	//第一页状态
	function toBeFirstPage() {
		$('.first').removeClass('hidden').siblings(".wrap-conent").addClass('hidden');
		$(".wrap-header li").eq(0).addClass("complete").siblings("li").removeClass('complete');
		$(".wrap-header li").eq(0).addClass("now").siblings("li").removeClass('now');
	}

	//第二页状态
	function toBeSecndPage() {
		$('.second').removeClass('hidden').siblings(".wrap-conent").addClass('hidden');
		$(".wrap-header li").eq(1).addClass("now").siblings("li").removeClass('now');
		$(".wrap-header li").eq(1).addClass("complete").next("li").removeClass('complete');
	}

	//第三页状态
	function toBeThirdPage() {
		$('.third').removeClass('hidden').siblings(".wrap-conent").addClass('hidden');
		$(".wrap-header li").eq(2).addClass("now").siblings("li").removeClass('now');
		$(".wrap-header li").addClass("complete");
	}


})