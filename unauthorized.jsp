<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <meta charset="utf-8">
    <link rel="stylesheet" href="static/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="static/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="static/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="static/css/simple-line-icons.css" type="text/css" />
    <link rel="stylesheet" href="static/css/font.css" type="text/css" />

    <title>权限不足</title>
</head>
<body class="alert alert-warning">
<div style="text-align: center; ">
    权限不足, 请使用管理员帐户登陆.
    <p>
        <a href="logout">重新登录</a>
    </p>
</div>
</body>
</html>